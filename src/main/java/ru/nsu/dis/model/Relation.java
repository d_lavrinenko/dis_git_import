package ru.nsu.dis.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.nsu.dis.converter.MemberConverter;

import javax.persistence.Convert;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity(name = "relation")
public class Relation extends OSMObject {

    @Convert(converter = MemberConverter.class)
    private List<Member> members = new ArrayList<>();

}
