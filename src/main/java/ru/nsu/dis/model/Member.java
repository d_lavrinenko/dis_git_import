package ru.nsu.dis.model;

import lombok.Data;

@Data
public class Member {

    private String type;

    private Long ref;

    private String role;
}