package ru.nsu.dis.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity(name = "node")
public class Node extends OSMObject {

    private Double lat;

    private Double lon;

}

