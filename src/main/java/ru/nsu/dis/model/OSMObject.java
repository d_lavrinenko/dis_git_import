package ru.nsu.dis.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLHStoreType;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.HashMap;
import java.util.Map;

@Data
@MappedSuperclass
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@TypeDef(name = "hstore", defaultForType = PostgreSQLHStoreType.class, typeClass = PostgreSQLHStoreType.class)
public class OSMObject {

    @Id
    private Long id;

    @Column(name = "node_user")
    private String user;

    private Long uid;

    private Boolean visible;

    private Long version;

    private Long changeset;

    private String timestamp;

    @Type(type = "hstore")
    @Column(columnDefinition = "hstore")
    private Map<String, String> tags = new HashMap<>();
}
