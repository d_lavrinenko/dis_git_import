package ru.nsu.dis.converter;

import ru.nsu.dis.model.Member;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class MemberConverter implements AttributeConverter<Member, String> {

    private static final String SEPARATOR = ", ";

    @Override
    public String convertToDatabaseColumn(Member attribute) {
        if (attribute == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        if (attribute.getType() != null && !attribute.getType().isEmpty()) {
            sb.append(attribute.getType());
        }
        sb.append(SEPARATOR);

        if (attribute.getRef() != null) {
            sb.append(attribute.getRef());
        }
        sb.append(SEPARATOR);

        if (attribute.getRole() != null && !attribute.getRole().isEmpty()) {
            sb.append(attribute.getRole());
        }
        sb.append(SEPARATOR);


        return sb.toString();
    }

    @Override
    public Member convertToEntityAttribute(String dbData) {
        if (dbData == null || dbData.isEmpty()) {
            return null;
        }

        String[] pieces = dbData.split(SEPARATOR);

        if (pieces.length == 0) {
            return null;
        }

        Member member = new Member();
        if (pieces.length == 3) {
            member.setType(pieces[0]);
            member.setRef(Long.parseLong(pieces[1]));
            member.setRole(pieces[2]);
            return member;
        } else {
            return null;
        }
    }
}
