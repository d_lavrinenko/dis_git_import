package ru.nsu.dis.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Converter
public class IntListConverter implements AttributeConverter<List<Integer>, String> {

    private static final String SEPARATOR = ", ";

    @Override
    public String convertToDatabaseColumn(List<Integer> list) {
        if (list == null) {
            return null;
        }

        return list.stream().map(integer -> integer.toString() + SEPARATOR).collect(Collectors.joining());
    }

    @Override
    public List<Integer> convertToEntityAttribute(String dbData) {
        if (dbData == null || dbData.isEmpty()) {
            return null;
        }

        String[] pieces = dbData.split(SEPARATOR);

        return Stream.of(pieces).map(Integer::parseInt).collect(Collectors.toList());
    }
}
