package ru.nsu.dis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.dis.model.Relation;
import ru.nsu.dis.repository.RelationRepository;

@RestController
@RequestMapping(path = "relation")
public class RelationController {

    private final RelationRepository relationRepository;

    @Autowired
    public RelationController(RelationRepository relationRepository) {
        this.relationRepository = relationRepository;
    }

    @GetMapping("get")
    public Relation getNodeById(@RequestParam Long id) {
        return relationRepository.getOne(id);
    }

    @GetMapping("getAll")
    public Page<Relation> getAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "100") int size) {
        return relationRepository.findAll(PageRequest.of(page, size));
    }

    @PostMapping("save")
    public Relation save(@RequestBody Relation relation) {
        return relationRepository.save(relation);
    }

    @DeleteMapping("delete")
    public void delete(@RequestParam Long id) {
        relationRepository.deleteById(id);
    }

    @DeleteMapping("deleteAll")
    public void delete() {
        relationRepository.deleteAll();
    }
}
