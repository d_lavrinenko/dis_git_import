package ru.nsu.dis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.dis.model.Way;
import ru.nsu.dis.repository.WayRepository;

@RestController
@RequestMapping(path = "way")
public class WayController {

    private final WayRepository wayRepository;

    @Autowired
    public WayController(WayRepository wayRepository) {
        this.wayRepository = wayRepository;
    }

    @GetMapping("get")
    public Way getNodeById(@RequestParam Long id) {
        return wayRepository.getOne(id);
    }

    @GetMapping("getAll")
    public Page<Way> getAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "100") int size) {
        return wayRepository.findAll(PageRequest.of(page, size));
    }

    @PostMapping("save")
    public Way save(@RequestBody Way way) {
        return wayRepository.save(way);
    }

    @DeleteMapping("delete")
    public void delete(@RequestParam Long id) {
        wayRepository.deleteById(id);
    }

    @DeleteMapping("deleteAll")
    public void delete() {
        wayRepository.deleteAll();
    }
}
