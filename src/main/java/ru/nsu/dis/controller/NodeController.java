package ru.nsu.dis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.dis.model.Node;
import ru.nsu.dis.repository.NodeRepository;
import ru.nsu.dis.service.PrepopulateService;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;

@RestController
@RequestMapping(path = "node")
public class NodeController {

    private final NodeRepository nodeRepository;
    private final PrepopulateService prepopulateService;

    @Autowired
    public NodeController(NodeRepository nodeRepository, PrepopulateService prepopulateService) {
        this.nodeRepository = nodeRepository;
        this.prepopulateService = prepopulateService;
    }

    @PostMapping("prepopulate")
    public void prepopulate() throws IllegalAccessException, XMLStreamException, IOException {
        prepopulateService.prepopulate();
    }

    @GetMapping("get")
    public Node getNodeById(@RequestParam Long id) {
        return nodeRepository.getOne(id);
    }

    @GetMapping("getAll")
    public Page<Node> getAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "100") int size) {
        return nodeRepository.findAll(PageRequest.of(page, size));
    }

    @PostMapping("save")
    public Node save(@RequestBody Node node) {
        return nodeRepository.save(node);
    }

    @DeleteMapping("delete")
    public void delete(@RequestParam Long id) {
        nodeRepository.deleteById(id);
    }

    @DeleteMapping("deleteAll")
    public void delete() {
        nodeRepository.deleteAll();
    }

    @GetMapping("nearest")
    public Page<Node> getNearest(@RequestParam double lat,
                                 @RequestParam double lon,
                                 @RequestParam double radius,
                                 @RequestParam(defaultValue = "0") int page,
                                 @RequestParam(defaultValue = "100") int size) {
        return nodeRepository.findAllByEarthDistance(lat, lon, radius, PageRequest.of(page, size));
    }
}

