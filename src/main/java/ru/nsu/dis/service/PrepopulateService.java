package ru.nsu.dis.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import ru.nsu.dis.model.Member;
import ru.nsu.dis.model.Node;
import ru.nsu.dis.model.OSMObject;
import ru.nsu.dis.model.Relation;
import ru.nsu.dis.model.Way;
import ru.nsu.dis.repository.NodeRepository;
import ru.nsu.dis.repository.RelationRepository;
import ru.nsu.dis.repository.WayRepository;
import ru.nsu.dis.stax.StaxStreamProcessor;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;

@Slf4j
@Service
public class PrepopulateService {

    @Value("${prepopulate.path}")
    private String prepopulatePath;

    private final ConversionService conversionService;

    private final NodeRepository nodeRepository;
    private final WayRepository wayRepository;
    private final RelationRepository relationRepository;

    @Autowired
    public PrepopulateService(ConversionService conversionService, NodeRepository nodeRepository,
                              WayRepository wayRepository, RelationRepository relationRepository) {
        this.conversionService = conversionService;
        this.nodeRepository = nodeRepository;
        this.wayRepository = wayRepository;
        this.relationRepository = relationRepository;
    }

    /**
     * Prepopulate database from file (path = ${prepopulate.path})
     */
    public void prepopulate() throws XMLStreamException, IOException, IllegalAccessException {
        log.info("Starting prepopulation");

        try (InputStream is = new FileInputStream(prepopulatePath);
             StaxStreamProcessor staxStreamProcessor = new StaxStreamProcessor(is)) {
            XMLStreamReader reader = staxStreamProcessor.getReader();

            while (reader.hasNext()) {
                reader.next();

                if (reader.isStartElement()) {
                    switch (reader.getLocalName()) {
                        case "node":
                            nodeRepository.save((Node) parseOSMObject(new Node(), reader));
                            break;
                        case "way":
                            wayRepository.save((Way) parseOSMObject(new Way(), reader));
                            break;
                        case "relation":
                            relationRepository.save((Relation) parseOSMObject(new Relation(), reader));
                            break;

                    }
                }
            }
        } catch (Exception ex) {
            log.error("Parse exception", ex);
            throw ex;
        }

        log.info("Prepopulation finished");
    }

    /**
     * Parse {@link OSMObject} from {@link XMLStreamReader}
     */
    private OSMObject parseOSMObject(OSMObject osmObject, XMLStreamReader reader) throws IllegalAccessException, XMLStreamException {

        fillFields(osmObject, reader);

        String osmObjectName = reader.getLocalName();
        while (XMLEvent.START_ELEMENT == reader.next() || !(reader.isEndElement() && osmObjectName.equals(reader.getLocalName()))) {
            if (reader.isStartElement()) {
                switch (reader.getLocalName()) {
                    case "member":
                        Relation relation = (Relation) osmObject;
                        relation.getMembers().add(fillFields(new Member(), reader));
                        break;
                    case "tag":
                        osmObject.getTags().put(reader.getAttributeValue(0), reader.getAttributeValue(1));
                        break;
                }
            }
        }

        return osmObject;
    }

    /**
     *  Fill single xml object's fields using reflection
     */
    private <T> T fillFields(T object, XMLStreamReader reader) throws IllegalAccessException {
        for (int i = 0; i < reader.getAttributeCount(); i++) {
            Field field = ReflectionUtils.findField(object.getClass(), reader.getAttributeLocalName(i));
            if (field != null) {
                boolean canAccess = field.canAccess(object);
                try {
                    field.setAccessible(true);
                    field.set(object, conversionService.convert(reader.getAttributeValue(i), field.getType()));
                } finally {
                    field.setAccessible(canAccess);
                }
            }
        }

        return object;
    }
}
