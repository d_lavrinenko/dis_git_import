package ru.nsu.dis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.dis.model.Relation;

/**
 * Implementation of {@link JpaRepository} to manage with {@link Relation}
 */
@Repository
public interface RelationRepository extends JpaRepository<Relation, Long> {
}
