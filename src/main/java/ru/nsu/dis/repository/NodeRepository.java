package ru.nsu.dis.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.nsu.dis.model.Node;

/**
 * Implementation of {@link JpaRepository} to manage with {@link Node}
 */
@Repository
public interface NodeRepository extends JpaRepository<Node, Long> {

    /**
     * Find nearest nodes to search point in radius.
     *
     * @param latitude  latitude of search point
     * @param longitude longitude of search point
     * @param radius    search radius
     * @param pageable  pageable
     * @return page of nearest {@link Node}
     */
    @Query(value = "SELECT * FROM Node WHERE earth_distance(ll_to_earth(lat, lon), ll_to_earth(:latitude, :longitude)) < :radius " +
            "ORDER BY earth_distance(ll_to_earth(lat, lon), ll_to_earth(:latitude, :longitude)) < :radius", nativeQuery = true)
    Page<Node> findAllByEarthDistance(@Param("latitude") Double latitude,
                                      @Param("longitude") Double longitude,
                                      @Param("radius") Double radius,
                                      Pageable pageable);
}
