package ru.nsu.dis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.dis.model.Way;

/**
 * Implementation of {@link JpaRepository} to manage with {@link Way}
 */
@Repository
public interface WayRepository extends JpaRepository<Way, Long> {
}
